DROP TABLE venue;
CREATE EXTERNAL TABLE venue
(
id int,
name varchar(30),
city varchar(30),
state varchar(30),
seatno int
)
ROW FORMAT DELIMITED FIELDS TERMINATED BY '|' LINES TERMINATED BY '\n'
STORED AS TEXTFILE
LOCATION 's3://somnath-dynamodb/data/input/';

INSERT OVERWRITE DIRECTORY 's3://somnath-dynamodb/data/output/' SELECT * FROM venue;
