Chef-Server
==============================================================
dpkg -i /tmp/chef-server-core-<version>.deb
chef-server-ctl reconfigure
chef-server-ctl user-create admin somnath mishra somnath.mishra@tcs.com 'admin123' --filename admin.pem
chef-server-ctl org-create chef-poc 'chef-poc' --association_user admin --filename chef-poc-validator.pem
chef-server-ctl reconfigure
chef-server-ctl install chef-manage
chef-server-ctl install opscode-reporting
chef-server-ctl reconfigure
chef-manage-ctl reconfigure [accept license and press 'q' at the end to quit]

Then download the pem file admin.pem and chef-poc-validator.pem and download starter kit from chef-management console and configure
the workstation.
============================================================
Chef-client [bootstrap] 
-----------------------------------------------------------------------
knife cookbook create helloworld
edit defult.rb in recipes and add following content

	file '/tmp/helloworld.txt' do
	  content 'hello world'
	end
knife cookbook upload helloworld	
knife bootstrap  54.209.230.169 -N devops-gitlab -E dev -r 'recipe[helloworld]' --hint ec2 --sudo -i  path-to-pem  -x
Note: Run with role/recipe

Chef-client [after bootstrap] 
-----------------------------------------------------------------------
curl -L https://www.chef.io/chef/install.sh | sudo bash -s -- -v 12.3.0 
Modify client.rb at node ssl_verify_mode :verify_none
sudo chef-client -r recipe[helloworld]
Note: Run with role/recipe
==========================================================