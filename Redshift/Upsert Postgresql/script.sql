--http://jakub.fedyczak.net/post/merge-upsert-example-in-postgresql-using-cte/
--TABLE-1
--=================
CREATE TABLE table1
(
  id integer,
  name character(20),
  pin character(20),
  contact character(20)
);
--=======================================
INSERT INTO table1(id, name, pin, contact) VALUES (3,'dilip kumar','1224562155454','5669874569');
INSERT INTO table1(id, name, pin, contact) VALUES (4,'partha pal','455689','85569874560');
INSERT INTO table1(id, name, pin, contact) VALUES (5,'subrata','89423','85569874560');


--TABLE2
--================
CREATE TABLE table2
(
  id integer,
  name character(20),
  pin character(20),
  contact character(20)
);
--======================================
INSERT INTO table2(id, name, pin, contact) VALUES (1,'somnath','723356','6998547896');
INSERT INTO table2(id, name, pin, contact) VALUES (2,'pankaj','569987','9335698745');
INSERT INTO table2(id, name, pin, contact) VALUES (3,'dilip','122456','5669874569');
INSERT INTO table2(id, name, pin, contact) VALUES (4,'partha','455689','8556987456');

--=====================================================================================
--UPSERT in POSTGRESQL
--============================================
WITH
    to_be_upserted (id,name,pin,contact) AS (
        select id,name,pin,contact from table1
    ),
    updated AS (
        UPDATE
            table2
        SET
            id = to_be_upserted.id,
			name=to_be_upserted.name,
			pin=to_be_upserted.pin,
			contact=to_be_upserted.contact
        FROM
            to_be_upserted
        WHERE
            table2.id = to_be_upserted.id
        RETURNING table2.id
    )
INSERT INTO table2
    SELECT * FROM to_be_upserted
    WHERE id NOT IN (SELECT id FROM updated);
--=====================================================================================

