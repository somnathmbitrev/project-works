http://tanaguru-sonar-plugin.readthedocs.io/en/master/run-with-eclipse/
http://docs.sonarqube.org/display/SONARQUBE44/SonarQube+in+Eclipse

> Download the plugin from- http://download.tanaguru.org/Tanaguru-sonar-plugin/sonar-eclipse.zip
> Then install from Eclipse Help --> Install New Software --> Browse zip after clicking Archive
> Configure the plugin
  Open the Preference dialog ("Window" => "Preferences"). A SonarQube entry is now present on left menu.
  Select the submenu "Preview Analysis Properties" and add the following properties :

   sonar.language with value accessibility
   sonar.sources with path to your files (multiple paths can be set, separated by a comma ',')
   
> This way, your plugin is set to perform accessibility analysis on all projects.
  To configure accessibility analysis only on some projects, right click on your project and select "Properties" (or Alt+Enter).
  As previously, a SonarQube entry is now present on left menu.
  You have to add the properties from this menu as described previously. The configuration is thus only applied on the current project.

> Perform analysis
  To perform analysis, right click on the project you want to test and select SonarQube => Analyze (or Ctrl+Alt+Q).
  
> Install PMD and Checkstyle from marketplace
> Install PMD and Checkstyle plugin in Sonar and add rules to Sonar qualtiy profiles