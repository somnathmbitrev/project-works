> SonarQube-cobertura-maven plugin for unit testing and integration testing using JACOCO

Upload the downloaded jar file in your SonarQube Server and put it in the directory : $SONARQUBE_HOME/extensions/plugins. Then restart the Sonarqube service.

Ref- http://downloads.sonarsource.com/plugins/org/codehaus/sonar-plugins/sonar-cobertura-plugin/1.6.3/sonar-cobertura-plugin-1.6.3.jar

> put following settings in pom.xml-

		<dependency>
			<groupId>org.codehaus.sonar-plugins.java</groupId>
			<artifactId>sonar-cobertura-plugin</artifactId>
			<version>1.3</version>
		</dependency>
		
		<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.2</version>
				<configuration>
					<source>1.7</source>
					<target>1.7</target>
				</configuration>
			</plugin>
			
			 <plugin><groupId>org.apache.maven.plugins</groupId><artifactId>maven-surefire-plugin</artifactId><configuration><runOrder>random</runOrder></configuration></plugin><plugin><groupId>org.codehaus.mojo</groupId><artifactId>sonar-maven-plugin</artifactId><version>2.7.1</version></plugin><plugin><groupId>org.jacoco</groupId><artifactId>jacoco-maven-plugin</artifactId><version>0.5.5.201112152213</version><configuration><destFile>${basedir}/target/jacoco-it.exec</destFile><dataFile>${basedir}/target/jacoco-it.exec</dataFile></configuration><executions><execution><id>jacoco-initialize</id><goals><goal>prepare-agent</goal></goals></execution><execution><id>jacoco-site</id><phase>package</phase><goals><goal>report</goal></goals></execution></executions></plugin>
	  
	  <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-site-plugin</artifactId>
            <version>3.0</version>
            <configuration>
                <reportPlugins>
                    <plugin>
                        <groupId>org.codehaus.mojo</groupId>
                        <artifactId>cobertura-maven-plugin</artifactId>
                        <version>2.5.1</version>
                        <configuration>
                            <formats>
                                <format>html</format>
                                <format>xml</format>
                            </formats>
                        </configuration>
                    </plugin>
                </reportPlugins>
            </configuration>
        </plugin>
		
	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<spring.version>4.0.7.RELEASE</spring.version>
	</properties>
	

> put following settings.xml under .m2 folder

<settings>
    <pluginGroups>
        <pluginGroup>org.sonarsource.scanner.maven</pluginGroup>
    </pluginGroups>
    <profiles>
        <profile>
            <id>sonar</id>
            <activation>
                <activeByDefault>true</activeByDefault>
            </activation>
            <properties>
                <!-- Optional URL to server. Default value is http://localhost:9000 -->
                <sonar.host.url>
                  http://localhost:9000/
                </sonar.host.url>
            </properties>
        </profile>
     </profiles>
</settings>	
======================================================
mvn eclipse:eclipse
mvn cobertura:cobertura
mvn site
mvn sonar:sonar