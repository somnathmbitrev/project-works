#! /bin/sh
# /etc/init.d/sonar
# chkconfig: 2345 95 20
#

# Some things that run always
# touch /var/lock/blah
SONAR_HOME=/home/ec2-user/sonarqube-4.5.5/bin/linux-x86-64
# Carry out specific functions when asked to by the system
case "$1" in
  start)
    echo "Starting sonar"
    $SONAR_HOME/sonar.sh start
    ;;
  stop)
    echo "Stopping sonar"
    $SONAR_HOME/sonar.sh stop
    ;;
  *)
    echo "Usage: /etc/init.d/blah {start|stop}"
    exit 1
    ;;
esac

exit 0
