Register EC2 inatance into opsworks stack
==================================================================
python --version
curl "https://s3.amazonaws.com/aws-cli/awscli-bundle.zip" -o "awscli-bundle.zip"
unzip awscli-bundle.zip
sudo ./awscli-bundle/install -i /usr/local/aws -b /usr/local/bin/aws

aws configure

aws opsworks register --infrastructure-class ec2 --region us-east-1 --stack-id ed1dc40b-bb26-4b92-a8aa-d21a609e1fd3 --local