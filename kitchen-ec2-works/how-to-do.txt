Install and setup kitchen EC2 to run chef on EC2
================================================================================
	$ gem install fog
	$ sudo yum install ruby*
	$ gem install kitchen-ec2
	
Do this in .kitchen directory
======	
    create a working directory in home and run after change to the directory
	$ kitchen init
	$ vim .kitchen.yml
	$ chmod 777 .kitchen.yml
	$ chmod 777 ../key-file/kol-somnath.pem
	$ mkdir createdir-ec2
	$ chmod 777 createdir-ec2
	$ cd createdir-ec2/
	$ vim metadata.rb
	$ chmod 777 metadata.rb
	$ mkdir recipes
	$ chmod 777 recipes/
	$ chmod 777 recipes
	$ cd recipes/
	$ vim default.rb
	$ chmod 777 default.rb
	$ kitchen converge [to launch instance with cookbook]
	$ kitchen destroy


==========================	
.kitchen.yml files content
==========================
---
driver:
  name: ec2
  aws_ssh_key_id: somnath
  ssh_key: /home/ec2-user/key-file/somnath.pem
  region: us-east-1
  availability_zone: us-east-1c
  require_chef_omnibus: true
  aws_access_key_id: <access key>
  aws_secret_access_key: <secret access key>
  security_group_ids: <security group name>

provisioner:
  name: chef_solo
  cookbook_path: "/home/ec2-user/chef-works/cookbooks"

platforms:
  - name: ubuntu-12.04
  - name: centos-6.4
    driver:
      image_id: ami-ed8e9284
      username: ec2-user

suites:
  - name: default
    run_list:
      - recipe[createdir-ec2::default]
    attributes:
	
********************** NOTE: Here the platform name is two so two ec2 will be provisioned
 
====================================
Content of default.rb file
====================================	
directory "/srv/www/shared" do
  mode 0755
  owner 'root'
  group 'root'
  recursive true
  action :create
end

=========================================
Then login to the instance

***** see the list of instance and login with instance name
	$ kitchen list
    $ chmod 600 ../key-file/somnath.pem
    $ kitchen login [instance name found from <kitchen list> command]