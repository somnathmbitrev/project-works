#
# Cookbook Name:: apache2
# Recipe:: default


package "apache2" do
  package_name "httpd"
  action :install
end

cookbook_file "/tmp/kitchen/cookbooks/apache2/files/default/index.html" do
  path "/var/www/html/index.html"
  mode '0777'
  owner 'ec2-user'
  group 'ec2-user'
  action :create_if_missing
end

service "httpd" do
  action :start
end  