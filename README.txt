CI/CD Process
--------------------------------------
> Raising A Pull Request

The method for rasing a pull request may differ between projects, so be sure to check the projects documentation for details. However, projects using 
GitHub will often use GitHub’s own tools for handling pull requests. A common workflow for submitting a pull request with GitHub would look like this:

1. Create/Log in to your GitHub account
2. Go to the page for the code respository you want to contribute to (the “upstream”)
3. “Fork” the repository (this creates a clone to your GitHub account)
4. Create a local clone of your fork with git clone
5. Create a local branch for your changes
6. Make your changes and commit them to your local branch with git commit, ensuring to include a descriptive commit message
7. Push the branch to your GitHub fork using git push
8. Go to the page for the upstream repository go to the pull requests tab
9. Click the “New Pull Request” Button
10. Select the branch you want to submit, and write a summary of what your change explaining what it is intended to do and how it is implemented

Install Weblogic
===========================================================================================================================
http://www.redwireservices.com/remote-x11-for-linux-unix

Install X11 forwarding server
----------------------------
# sudo yum install xorg-x11-xauth xterm
# service sshd restart

java -jar -Xmx1024m -XX:MaxPermSize=512M Wls-setup.jar

======================================================================================================================================================
Autodeployment weblogic 
===========================================================================================================================
http://stackoverflow.com/questions/19804781/how-to-deploy-war-ear-file-from-command-line-in-weblogic

set CLASSPATH=C:\Oracle\Middleware\Oracle_Home\wlserver\server\lib\weblogic.jar

Local deployment
-----------------
java weblogic.Deployer -adminurl t3://localhost:7001 -username weblogic -password weblogic123 -deploy -name sample -targets AdminServer -source  C:\Users\xy14107\Desktop\sample.war

Remote Deployment
-----------------
java weblogic.Deployer -adminurl t3://54.152.116.196:7001 -username weblogic -password weblogic123 -deploy -name sample -targets AdminServer -source  C:\Users\xy14107\Desktop\sample.war -remote -upload

===========================================================================================================================
Sonarqube Analysis
===========================================================================================================================

  http://localhost:9000/

> Others report by default will come to dashboard-

> To populate test results in SONAR-JECOCO
  http://docs.sonarqube.org/display/PLUG/Usage+of+JaCoCo+with+Java+Plugin

# mvn clean org.jacoco:jacoco-maven-plugin:prepare-agent install -Dmaven.test.failure.ignore=true
# mvn sonar:sonar
=========================================================================================================================
AWS codecommit jenkins
=========================================================================================================================
http://docs.aws.amazon.com/codecommit/latest/userguide/setting-up-https-windows.html#setting-up-https-windows-credential-helper
> AWS configure
> Set Up the Credential Helper

then add clone URL and username,password with add credential.

Note: May got 403 error
=======================================================================================================================