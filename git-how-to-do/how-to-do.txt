==================================================================
Github commands-
======================
***** Create repository add to remote repository 

first create repository into github and from your local repository execute below command

	touch README.md
	git init
	git add README.md [for all command => git add -A]
	git commit -m "first commit"
	git remote add origin <repository URL from github after creating from web>
	git push -u origin master

**** Clone an existing repository

	git clone <HTTPS URL>
	
**** After modifying a file commit into repository
	git commit readme.txt -m 'commit okk'

**** Push into origin i.e remote github repo
	git push origin	

**** removing files 
    git rm <filename>
	git commit readme.txt
	git push origin master
	
**** Important Task
===================
**** Add a new file into repository
	git add Readme.txt
	git commit <file name> -m '<message>'

**** see last commit
	git branch -v

**** Create a branch
	git branch dev

**** List branches
	git branch -v
	git checkout master (switch to master branch)
	
**** Create local branch. switch to the branch. change the file and commit
	$ git branch devlocal
	$ git checkout devlocal
	$ git commit Readme.txt
	$ git push origin devlocal:dev [push from local to remote]
	
**** Pull a specific branch
	$ git pull origin dev
	
**** Revert files to last commit
	$ git reset --hard HEAD
	
**** Merge into dev to master branch then push to remote

	$ git checkout master
	$ git merge dev
	$ git push origin
	
**** Remove a local branch	
    $ git branch -d <branch-name>

**** Create a Remote Branch
    $ git checkout -b dev
	$ git push origin<remote-name>  dev<new branch name>

**** Remove remote branch
	$ git push origin --delete dev
	
**** Deleting a local remote-tracking branch:

	$ git branch -dr <remote>/<branch>	
	
**** Commit history of a file

	$ git log -p Readme.txt

**** Git submodule add 

git submodule add https://github.com/svanzoest-cookbooks/apache2.git
git submodule init
cd apache2
git checkout master
git pull
cd ..
git commit -am "Pulled down update to submodule_dir"
git push origin master
==============================================================================================================
***************Some hands on
==============================================================================================================
git create remote branch
----------------------
	$ git checkout -b qa
	$ git push origin qa

----------------------------
Merge dev to qa
----------------------------
	$ git checkout dev
	$ git push origin dev:qa

**** If qa is parent branch of dev then-

	$ git checkout qa
	$ git merge dev
	$ git push origin qa:qa

----------------------------
Git merge conflicts from 
<test2> remote branch to <test2> local branch
Note: If git pull error occurred
----------------------------
	$ git stash
	$ git merge origin/test2
	$ git stash pop
----------------------------
To delete a local branch
----------------------------

	$ git branch -D <the_local_branch>

----------------------------
To remove a remote branch
----------------------------
	$ git push origin :the_remote_branch

----------------------------
Create Tag from master branch
----------------------------
	$ git checkout master
	$ git tag 1.0
	$ git push origin 1.0
	$ git fetch 1.0

-----------------------------
Delete tag
-----------------------------
	$ git tag -d 1.0
	$ git push origin :refs/tags/1.0