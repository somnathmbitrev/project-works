Setup NFS Server
==========================
Check NFS/Xinetd/Portmap is installed there or not?
==========================
# rpm -qa nfs*
# rpm -qa portmap*
# rpm -qa xinetd*

If not install below packages
==========================
# sudo yum install nfs*
# sudo yum install portmap*
# sudo yum install xinetd*
==========================
# service rpcbind restart
# service xinetd restart
# service nfs restart
# mkdir /datafornfs
# chmod 777 /datafornfs
# vi /etc/exports
    please enter a line==> /datafornfs       20.0.0.0/24(rw,sync)

# service nfs restart
# chkconfig nfs on
# chkconfig xinetd on
# chkconfig rpcbind on 	
# exportfs -R
# showmount -e localhost

==============================================
NFS client setup
================================
******Note- 20.0.0.22 is the NFS server IP

# showmount -e 20.0.0.22
# mount -t nfs 20.0.0.22:/datafornfs /data1	
# mkdir /data1

To keep it available after reboot make its entry in fstab
==================
# vi /etc/fstab
     and enter a new line like below
	 
     20.0.0.22:/datafornfs      /data1       nfs     defaults    0    0