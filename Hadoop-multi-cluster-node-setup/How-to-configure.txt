Ref: http://tecadmin.net/set-up-hadoop-multi-node-cluster-on-centos-redhat/#
     https://www.digitalocean.com/community/tutorials/how-to-install-hadoop-on-ubuntu-13-10
	 http://hadoop.apache.org/docs/current/hadoop-project-dist/hadoop-common/FileSystemShell.html#put
	 http://hadoop.apache.org/docs/current/hadoop-mapreduce-client/hadoop-mapreduce-client-core/MapReduceTutorial.html#Example:_WordCount_v1.0
	 
Step 1. Create User Account

    Create a system user account on both master and slave systems to use for hadoop installation-

    # useradd hadoop
    # passwd hadoop
	
Step 2. Install Java

    Before installing hadoop make sure you have java installed on all nodes of hadoop cluster systems.

    # java -version

    java version "1.7.0_75"
    Java(TM) SE Runtime Environment (build 1.7.0_75-b13)
    Java HotSpot(TM) 64-Bit Server VM (build 24.75-b04, mixed mode)

    If you do not have java installed use following article to install Java.

    # yum -y install *openjdk*

	1.1)  Editing /home/hadoop/.bashrc

	Before editing the .bashrc file in your home directory, we need to find the path where Java has been installed to set the JAVA_HOME 
	environment variable. Let's use the following command to do that:

	# update-alternatives --config java
	
	This will display something like the following:
	Get Java Path
	
	The complete path displayed by this command is:
	
	/usr/lib/jvm/java-7-openjdk-amd64/jre/bin/java
	
	The value for JAVA_HOME is everything before /jre/bin/java in the above path - in this case, /usr/lib/jvm/java-7-openjdk-amd64. Make a 
	note of this as we'll be using this value in this step and in one other step.

	Now use nano (or your favored editor) to edit ~/.bashrc using the following command:

	# nano ~/.bashrc
	
	This will open the .bashrc file in a text editor. Go to the end of the file and paste/type the following content in it:
	
	#HADOOP VARIABLES START
	export JAVA_HOME=/usr/lib/jvm/java-1.7.0
	export PATH=$PATH:$JAVA_HOME/bin
	export HADOOP_INSTALL=/opt/hadoop/hadoop
	export PATH=$PATH:$HADOOP_INSTALL/bin
	export PATH=$PATH:$HADOOP_INSTALL/sbin
	export HADOOP_MAPRED_HOME=$HADOOP_INSTALL
	export HADOOP_COMMON_HOME=$HADOOP_INSTALL
	export HADOOP_HDFS_HOME=$HADOOP_INSTALL
	export YARN_HOME=$HADOOP_INSTALL
	export HADOOP_COMMON_LIB_NATIVE_DIR=$HADOOP_INSTALL/lib/native
	export HADOOP_OPTS="-Djava.library.path=$HADOOP_INSTALL/lib"
	export HADOOP_CLASSPATH=${JAVA_HOME}/lib/tools.jar
	#HADOOP VARIABLES END

	Note 1: If the value of JAVA_HOME is different on your VPS, make sure to alter the first export statement in the above content accordingly.
		Note 2: Files opened and edited using nano can be saved using Ctrl + X. Upon the prompt to save changes, type Y. If you are asked for 
		a filename, just press the enter key.

		After saving and closing the .bashrc file, execute the following command so that your system recognizes the newly created environment 
		variables:

		# source .bashrc
		
		Putting the above content in the .bashrc file ensures that these variables are always available when your VPS starts up.
    
Step 3: Add FQDN Mapping
	
	Resolve hostname from master and slaves and map those hostname-
	$ hostname
	
    Edit /etc/hosts file on all master and slave servers and add following entries.

    # vim /etc/hosts
    
    172.31.25.9 i.e<master node ip> ip-172-31-25-9
    172.31.25.10 i.e<slave node ip> ip-172-31-25-10
    .
    .
    .
    172.31.25.100 hadoop-slave-n
    
Step 4. Configuring Key Based Login

    It’s required to set up hadoop user to ssh itself without password. Use following commands to configure auto login 
    between all hadoop cluster servers..

    # su - hadoop
    $ ssh-keygen -t rsa
    $ cat .ssh/id_rsa.pub

    Then paste the key to this master server /home/hadoop/.ssh/authorized_keys and slaves /home/hadoop/.ssh/authorized_keys. Also add the id_rsa.pub
    of slave into master nodes /home/hadoop/.ssh/authorized_keys and in itself means in slave nodes /home/hadoop/.ssh/authorized_keys.

    $ chmod 0600 ~/.ssh/authorized_keys
    $ exit    

    Test for ssh from master to master node and slave.
    Master --> Slave/Master

    $ ssh hadoop@ip-172-31-25-9
    $ ssh hadoop@ip-172-31-25-10

    Test for ssh from slave to master node and slave.
    Slave --> Master/Slave

    $ ssh hadoop@ip-172-31-25-9
    $ ssh hadoop@ip-172-31-25-10
    
Step 5. Download and Extract Hadoop Source

    Download hadoop latest available version from its official site at hadoop-master server only.

    # mkdir /opt/hadoop
    # cd /opt/hadoop/
    # wget http://apache.mesi.com.ar/hadoop/common/hadoop-2.7.1/hadoop-2.7.1.tar.gz
    # tar -xzf hadoop-2.7.1.tar.gz
    # mv hadoop-2.7.1 hadoop
    # chown -R hadoop /opt/hadoop
    # cd /opt/hadoop/hadoop/    
    
Step 6: Configure Hadoop
    
    # cd /opt/hadoop/hadoop
    
    First edit hadoop configuration files and make following changes.
    6.1) Edit core-site.xml    
    
         # vi etc/hadoop/core-site.xml
         
    Add the following inside the configuration tag
    
        <property>
            <name>fs.default.name</name>
            <value>hdfs://172.31.25.9:9000/</value>
        </property>
        <property>
            <name>dfs.permissions</name>
            <value>false</value>
        </property>
        
    6.2) Edit hdfs-site.xml
	
	# mkdir /opt/hadoop/hadoop_store/hdfs/namenode
	# mkdir /opt/hadoop/hadoop_store/hdfs/datanode
    # vim etc/hadoop/hdfs-site.xml

    Add the following inside the configuration tag

   <property>
           <name>dfs.replication</name>
           <value>1</value>
         </property>
         <property>
           <name>dfs.namenode.name.dir</name>
           <value>file:/opt/hadoop/hadoop_store/hdfs/namenode</value>
         </property>
         <property>
           <name>dfs.datanode.data.dir</name>
           <value>file:/opt/hadoop/hadoop_store/hdfs/datanode</value>
   </property>
      
	# mkdir /opt/hadoop/hadoop_store 
	# chown hadoop:hadoop /opt/hadoop/hadoop_store
    6.3) Edit mapred-site.xml    
        
    # cp etc/hadoop/mapred-site.xml.template etc/hadoop/mapred-site.xml          
    # vi etc/hadoop/mapred-site.xml   
    
	Add the following inside the configuration tag

	<property>
		<name>mapred.job.tracker</name>
		<value>ip-172-31-25-9:9001</value>
	</property>    

	6.4) Edit hadoop-env.sh

	# vim etc/hadoop/hadoop-env.sh
	
	export JAVA_HOME=/usr/lib/jvm/jre-1.7.0-openjdk.x86_64/
	export HADOOP_OPTS=-Djava.net.preferIPv4Stack=true
	export HADOOP_CONF_DIR=/opt/hadoop/hadoop/etc/hadoop

	6.5) Editing etc/hadoop/yarn-site.xml

	The /usr/local/hadoop/etc/hadoop/yarn-site.xml file contains configuration properties that MapReduce uses when starting up. 
	This file can be used to override the default settings that MapReduce starts with.

	Open this file with nano using the following command:

	# vi etc/hadoop/yarn-site.xml
	
	In this file, enter the following content in between the <configuration></configuration> tag:

		<property>
		   <name>yarn.nodemanager.aux-services</name>
		   <value>mapreduce_shuffle</value>
		</property>
		<property>
		   <name>yarn.nodemanager.aux-services.mapreduce.shuffle.class</name>
		   <value>org.apache.hadoop.mapred.ShuffleHandler</value>
		</property>

	Save and close this file.

Step 7. Copy Hadoop Source to Slave Servers from Master

	After updating above configuration, we need to copy the source files to all slaves servers.
	
	Execute the followings in all the slaves before scp
	
	# mkdir /opt/hadoop
    # chown hadoop:hadoop /opt/hadoop
	
	# su - hadoop
	$ cd /opt/hadoop
	$ scp -r hadoop ip-172-31-25-10:/opt/hadoop
	
Step 8. Configure Hadoop on Master Server Only

	Go to hadoop source folder on hadoop-master and do following settings.

	# su - hadoop
	$ cd /opt/hadoop/hadoop
	$ vim etc/hadoop/masters

		ip-172-31-25-9
		
	$ vim etc/hadoop/slaves
	
       ip-172-31-25-10

	Format Name Node on Hadoop Master only

	# su - hadoop
	$ cd /opt/hadoop/hadoop
	$ bin/hadoop namenode -format

Step 9. Start Hadoop Services

	Use the following command to start all hadoop services on Hadoop-Master only

	$ sbin/start-all.sh
    
	Then start the datanode service in master with following command-
	$ sbin/hadoop-daemon.sh start datanode
	
	[master]$ jps
	14760 NameNode
	14230 ResourceManager
	15394 Jps
	14080 SecondaryNameNode
	15115 DataNode

	
	[slave] $ jps
	15159 Jps
	14814 DataNode
	14953 NodeManager

Step 10. Open following in your browser to check hadoop master is running or not-
    http://localhost:8088/ 
	
Notes: Remove temp if necessary

# rm -rf /tmp/*
# rm -rf /tmp/*.*

===============================================================
Wordcount example with java
===============================================================
# vi WordCount.java
	import java.io.IOException;
	import java.util.StringTokenizer;

	import org.apache.hadoop.conf.Configuration;
	import org.apache.hadoop.fs.Path;
	import org.apache.hadoop.io.IntWritable;
	import org.apache.hadoop.io.Text;
	import org.apache.hadoop.mapreduce.Job;
	import org.apache.hadoop.mapreduce.Mapper;
	import org.apache.hadoop.mapreduce.Reducer;
	import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
	import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

	public class WordCount {

	  public static class TokenizerMapper
		   extends Mapper<Object, Text, Text, IntWritable>{

		private final static IntWritable one = new IntWritable(1);
		private Text word = new Text();

		public void map(Object key, Text value, Context context
						) throws IOException, InterruptedException {
		  StringTokenizer itr = new StringTokenizer(value.toString());
		  while (itr.hasMoreTokens()) {
			word.set(itr.nextToken());
			context.write(word, one);
		  }
		}
	  }

	  public static class IntSumReducer
		   extends Reducer<Text,IntWritable,Text,IntWritable> {
		private IntWritable result = new IntWritable();

		public void reduce(Text key, Iterable<IntWritable> values,
						   Context context
						   ) throws IOException, InterruptedException {
		  int sum = 0;
		  for (IntWritable val : values) {
			sum += val.get();
		  }
		  result.set(sum);
		  context.write(key, result);
		}
	  }

	  public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
		Job job = Job.getInstance(conf, "word count");
		job.setJarByClass(WordCount.class);
		job.setMapperClass(TokenizerMapper.class);
		job.setCombinerClass(IntSumReducer.class);
		job.setReducerClass(IntSumReducer.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(IntWritable.class);
		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));
		System.exit(job.waitForCompletion(true) ? 0 : 1);
	  }
	}

# ../hadoop/bin/hadoop com.sun.tools.javac.Main WordCount.java
# jar cf wc.jar WordCount*.class	

===================================================
Create directory in HDFS-
$  bin/hadoop fs -mkdir -p testing
 
To List the directory in HDFS
$ bin/hadoop fs -ls

$ bin/hadoop fs -mkdir -p testing/input
$ bin/hadoop fs -mkdir -p testing/output
$ bin/hadoop fs -ls testing
$ vi /home/hadoop/file01
	Hello World Bye World
$ bin/hadoop fs -copyFromLocal /home/hadoop/file01 testing/input/file01
$ vi /home/hadoop/file02
   Hello Hadoop Goodbye Hadoop
   
$ bin/hadoop fs -copyFromLocal /home/hadoop/file02 testing/input/file02  

To see the contents of file01 and file02

$ bin/hadoop fs -cat testing/input/file01
$ bin/hadoop fs -cat testing/input/file02

To append contents of a file from local to HDFS-

$ bin/hadoop fs -appendToFile /home/hadoop/file01 testing/input/file01

Remove non empty output directory if created -
$ bin/hadoop fs -rm -r testing/output9

--------------------------------------------------
Now run the JAVA program WordCount.java
	
$ bin/hadoop jar ../workspace/wc.jar WordCount testing/input testing/output

Note: If there is namenode safemode on the off this with following command-
$ hdfs dfsadmin -safemode leave
---------------------------------------

See the output from Master/Slave node-
List the output files-
$ bin/hadoop fs -ls testing/output

Execute the following to show the output result

$ bin/hadoop fs -cat testing/output/part-r-00000
	
	
Install Pig
----------------------------
Go to http://www.eu.apache.org/dist/pig/pig-0.15.0/
# wget http://www.eu.apache.org/dist/pig/pig-0.15.0/pig-0.15.0.tar.gz
# tar -zxf pig-0.15.0.tar.gz
# mv pig-0.15.0.tar.gz pig
# vi ~/.bashrc

	export PIG_HOME=/opt/hadoop/pig
	export PATH=$PIG_HOME/bin:$PATH

To test the installation is successful or not 
# pig -help

To open pig in Mapreduce Mode execute following command
# pig

Pig Word count example
-------------------------------------------------------------
# pig
	grunt> a = load 'testing/input';
	grunt> b = foreach a generate flatten(TOKENIZE((chararray)$0)) as word;
	grunt> c = group b by word;
	grunt> d = foreach c generate COUNT(b), group;
	grunt> store d into 'testing/output/pig_wordcount';
	grunt> quit

Then you can see the list of output files-

# bin/hadoop fs -ls testing/output/pig_wordcount

You can see the word counts by following
# bin/hadoop fs -cat testing/output/pig_wordcount/part-r-00000

===========================================================================================================
Install Hive
========================================================================================
Please go to http://www.us.apache.org/dist/hive/hive-1.2.1/
# wget http://www.us.apache.org/dist/hive/hive-1.2.1/apache-hive-1.2.1-bin.tar.gz
# tar -zxvf apache-hive-1.2.1-bin
# mv apache-hive-1.2.1-bin hive

1. Setting up environment for Hive
===================
You can set up the Hive environment by appending the following lines to ~/.bashrc file:

$ vi ~/.bashrc
	export HIVE_HOME=/opt/hadoop/hive
	export PATH=$PATH:$HIVE_HOME/bin
	export CLASSPATH=$CLASSPATH:$HIVE_HOME/lib/*:.
	
2. Configuring Hive
===================
To configure Hive with Hadoop, you need to edit the hive-env.sh file, which is placed in the $HIVE_HOME/conf directory. 
The following commands redirect to Hive config folder and copy the template file:

$ cd $HIVE_HOME/conf
$ cp hive-env.sh.template hive-env.sh	
$ vi hive-env.sh
	export HADOOP_HOME=/opt/hadoop/hadoop
	
3. Downloading and Installing MySQL and create metastore
=========================================
Ref: https://dzone.com/articles/how-configure-mysql-metastore
---------------------------------------
# sudo yum -y update
# sudo yum -y install mysql-server
# service mysqld start

securing Mysql
# sudo /usr/bin/mysql_secure_installation

# chkconfig mysqld on

$ mysql -u root -p
Enter password:
mysql> CREATE DATABASE metastore;
mysql> USE metastore;

mysql> CREATE USER 'hiveuser'@'%' IDENTIFIED BY 'hivepassword'; 
mysql> GRANT all on *.* to 'hiveuser'@localhost identified by 'hivepassword';
mysql>  flush privileges;
-------------------------------------------------

Then do the following configuration

$ vi hive-site.xml [add following section]

<property>
      <name>javax.jdo.option.ConnectionURL</name>
      <value>jdbc:mysql://localhost:3306/metastore?createDatabaseIfNotExist=true</value>
      <description>metadata is stored in a MySQL server</description>
   </property>
   <property>
      <name>javax.jdo.option.ConnectionDriverName</name>
      <value>com.mysql.jdbc.Driver</value>
      <description>MySQL JDBC driver class</description>
   </property>
   <property>
      <name>javax.jdo.option.ConnectionUserName</name>
      <value>hiveuser</value>
      <description>user name for connecting to mysql server</description>
   </property>
   <property>
      <name>javax.jdo.option.ConnectionPassword</name>
      <value>hivepassword</value>
      <description>password for connecting to mysql server</description>
   </property>
   
   
 and comment out following section-
 
		<property>
		<name>hive.exec.local.scratchdir</name>
		<value>${system:java.io.tmpdir}/${hive.xxxxx.xxxx}</value>
		<description>Local scratch space for Hive jobs</description>
	  </property>
	  <property>
		<name>hive.downloaded.resources.dir</name>
		<value>${system:java.io.tmpdir}/${hive.session.id}_resources</value>
		<description>Temporary local directory for added resources in the remote file system.</description>
	  </property>
	  
$ vi conf/jpox.properties
	  
	javax.jdo.PersistenceManagerFactoryClass=org.jpox.PersistenceManagerFactoryImpl
	org.jpox.autoCreateSchema=false
	org.jpox.validateTables=false
	org.jpox.validateColumns=false
	org.jpox.validateConstraints=false
	org.jpox.storeManagerType=rdbms
	org.jpox.autoCreateSchema=true
	org.jpox.autoStartMechanismMode=checked
	org.jpox.transactionIsolation=read_committed
	javax.jdo.option.DetachAllOnCommit=true
	javax.jdo.option.NontransactionalRead=true
	javax.jdo.option.ConnectionDriverName=com.mysql.jdbc.Driver
	javax.jdo.option.ConnectionURL=jdbc:mysql://localhost:3306/metastore?createDatabaseIfNotExist=true
	javax.jdo.option.ConnectionUserName=hiveuser
	javax.jdo.option.ConnectionPassword=hivepassword

4. Verifying Hive Installation
============================
Before running Hive, you need to create the /tmp folder and a separate Hive folder in HDFS. Here, we use the /opt/hadoop/hive/warehouse folder.
You need to set write permission for these newly created folders as shown below:

chmod g+w
Now set them in HDFS before verifying Hive. Use the following commands:

$ $HADOOP_HOME/bin/hadoop fs -mkdir /tmp 
$ $HADOOP_HOME/bin/hadoop fs -mkdir -p testing/hive/warehouse
$ $HADOOP_HOME/bin/hadoop fs -chmod g+w /tmp 
$ $HADOOP_HOME/bin/hadoop fs -chmod g+w testing/hive/warehouse

The following commands are used to verify Hive installation:

$ cd $HIVE_HOME
$ bin/hive

========================================================================================================
========================================================================================================
How to launch from AMIs
========================================================================================================
> Launch both the instances from AMIs [ami-5f68213a <master-node>, ami-8f6821ea <data-node>]
> Modify hosts file in both master and slave
<master ip> <hostname>
<slave ip> <hostname>

like following-

172.31.30.41 ip-172-31-30-41
172.31.28.112 ip-172-31-28-112

> Login as hadoop with password hadoop

> vi etc/hadoop/core-site.xml

	modify with master node ip

> vi etc/hadoop/mapred-site.xml 

	modify with master node host name

> vim etc/hadoop/masters
	modify with master node host name
	
> vim etc/hadoop/slaves
	modify with all slaves host name
	
> scp -r hadoop <slave node host name>:/opt/hadoop	[run this from master like following]

	scp -r hadoop ip-172-31-28-112:/opt/hadoop
	
> Start Hadoop Services
	$ sbin/start-all.sh	
	$ sbin/hadoop-daemon.sh start datanode
	
> To start hive use following command
  $ hive	
> To start pig use following command
  $ pig
==============================================================================================================  